package com.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HomeController {

	@GetMapping("/")
    public String home(ModelMap model) {
//		passing a message attribute to the JSP file
		model.addAttribute("message","hello spring");
        return "/home";
    }
	
	@GetMapping("/index")
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView("/index");
		modelAndView.addObject("message", "success");
		return modelAndView;
		
	}

}
